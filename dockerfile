FROM python:latest
WORKDIR /usr.src.app
RUN python -m pip install 
COPY helloworld.py
CMD [ "python", "helloworld.py" ]
